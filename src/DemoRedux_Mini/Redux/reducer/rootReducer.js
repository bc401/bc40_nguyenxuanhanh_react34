import {combineReducers} from "redux";
import { numberReducer } from "./numberReducer";


export const rootReducer_DemoReduxMini = combineReducers({
    numberReducer: numberReducer
})