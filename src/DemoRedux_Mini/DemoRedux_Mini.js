import React, { Component } from 'react'
import { connect } from 'react-redux'

 class DemoRedux_Mini extends Component {
    
  render() {
    console.log(this.props)
    return (
      <div className='container'>
            <button onClick={this.props.handleGiamsoluong} className='btn btn-success'>-</button>
            <strong className='mx-5'>{this.props.soluong}</strong>
            <button onClick={this.props.handleTangsoluong} className='btn btn-success'>+</button>
      </div>
    )
  }
}

let mapStateProps = (state) => {
    return {
        soluong: state.numberReducer.number,
    }
}

let mapDispatchToProps= (dispatch) => {
    return {
        handleTangsoluong: () => {
            let action = {
                type: "TANG_SO_LUONG", 
            }
            dispatch(action);
        },
        handleGiamsoluong: () => {
            let action = {
                type: "GIAM_SO_LUONG", 
            }
            dispatch(action);
        }
    }
}
export default connect(mapStateProps, mapDispatchToProps)(DemoRedux_Mini);