import logo from './logo.svg';
import './App.css';
import DemoRedux_Mini from './DemoRedux_Mini/DemoRedux_Mini';

function App() {
  return (
    <DemoRedux_Mini/>
  );
}

export default App;
